# Zabbix-disk-image

### Prepare:

* Under Ubuntu/Debian:  
    * `./prepare-buildenv-ubuntu`
* Under CentOS:  
    * `yum install kpartx`
* For image testing:  
    * `apt install qemu-kvm || yum install qemu-kvm`

### Build:

* ROOT_PASSWORD will be used for login locally and via SSH:  
    * `export ROOT_PASSWORD=123`
    * `./zabbix-disk-image build`

### Testing:

* In first console:  
    * `./centos-disk-image kvm`
* In second console:  
    * `sudo ip addr add 192.168.0.100/24 dev tap1`
    * `ping -w1 192.168.0.1`
    * `ssh root@192.168.0.1`
* In Web browser:
    * open http://192.168.0.1/zabbix
    * login "Admin"
    * password "zabbix"
